import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestPlane {
    @Test
    public void aPlaneID(){
        Plane plane = new Plane(1234);
        assertEquals(plane.getPlaneID(), 1234); 
    }

}
