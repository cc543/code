import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import org.junit.Test;

public class TestPerson {
    @Test
    public void aPersonsName(){
        Person person = new Person("Bob");
        assertEquals(person.getName(), "Bob"); 
    }
    @Test
    public void aPersonCanHaveABoardingPass(){
        Person person = new Person("Bob");
        BoardingPass pass = new BoardingPass(1,1);
        person.setBoardingPass(pass);
        assertSame(person.getBoardingPass(), pass);
    }

    
}

