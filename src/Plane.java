public class Plane {
    private int planeID;

    public Plane(int planeID){
        this.planeID = planeID;
    }

    public int getPlaneID(){
        return planeID;
    }
}